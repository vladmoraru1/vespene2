#!/bin/bash

# run this command in the root level of the repository with ./scripts/dump_database.sh
# ignore the error about changing directory, it's supposed to say that

# Set these variables for the ssh key you use and the name you want to give the backup file
key_path="~/.aws/TommyNV.pem"
backupname="vespene_db.bak"

vespene_ip=$(terraform output vespene_public_ip)

# if the backup file already exists, which probably launched the existing machine and we know works, then rename it as the backup for the backup
if [ -f modules/vespene/config/vespene_db.bak ]
then
  mv modules/vespene/config/$backupname modules/vespene/config/${backupname}.backup
fi

# get the backup
ssh -o StrictHostKeyChecking=no -i $key_path ec2-user@${vespene_ip} \
"sudo -u postgres pg_dump vespene > ${backupname}"

# copy to the relevant directory
scp -i $key_path ec2-user@${vespene_ip}:~/${backupname} modules/vespene/config/${backupname}
