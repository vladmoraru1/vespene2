#!/bin/#!/usr/bin/env bash

# source mkenv

TF_VAR_access_key=$(grep -i 'aws_access_key' ~/.aws/credentials | awk '{print $NF}')

TF_VAR_secret_key=$(grep -i 'aws_secret_access_key' ~/.aws/credentials | awk '{print $NF}')

TF_VAR_username=$(grep -i 'username' credentials | awk '{print $NF}')

TF_VAR_email=$(grep -i 'email' credentials | awk '{print $NF}')

TF_VAR_password=$(grep -i 'password' credentials | awk '{print $NF}')

export TF_VAR_access_key TF_VAR_secret_key TF_VAR_username TF_VAR_email TF_VAR_password
