resource "aws_security_group" "jenkins_sg" {
    name = "${var.environment}-jenkins-sg"
    description = "jenkins security group"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${var.vpc_id}"

    tags {
      Name        = "${var.environment}-jenkins-sg"
      Environment = "${var.environment}"
    }
}
