resource "aws_iam_role" "jenkinsrole" {
  name = "${var.environment}_jenkins_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "jenkins_profile" {
  name = "${var.environment}_jenkins_profile"
  role = "${aws_iam_role.jenkinsrole.name}"
}

resource "aws_iam_role_policy_attachment" "jenkins_ec2" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_role_policy_attachment" "jenkins_s3" {
  role       = "${aws_iam_role.jenkinsrole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
