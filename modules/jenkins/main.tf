resource "aws_instance" "jenkins" {
    ami = "${var.image_id}"
    vpc_security_group_ids = [ "${aws_security_group.jenkins_sg.id}" ]
    instance_type = "${var.instance_type}"
    key_name      = "${var.key_name}"
    associate_public_ip_address = true
    iam_instance_profile = "${aws_iam_instance_profile.jenkins_profile.name}"
    tags {
        Name = "${var.environment}-jenkins"
    }
    subnet_id = "${element(var.public_subnets_id, count.index)}"

    depends_on = ["aws_instance.jenkins"]

    connection {
      user         = "ec2-user"
      private_key  = "${file("~/.aws/vlad.pem")}"
    }

    provisioner "file" {
      source = "${path.module}/config/setup"
      destination = "/home/ec2-user/setup"
    }

    provisioner "file" {
      source = "${path.module}/config/config.xml"
      destination = "/home/ec2-user/config.xml"
    }

    provisioner "remote-exec" {
      inline = [
        "chmod +x setup",
        "./setup"
      ]
    }

    provisioner "local-exec" {
      command = "echo \"[jenkins]\" > ${path.module}/ansible/hosts"
    }

    provisioner "local-exec" {
      command = "echo ${aws_instance.jenkins.public_ip} >> ${path.module}/ansible/hosts"
    }

    provisioner "local-exec" {
      command     = "ansible-playbook -i ${path.module}/ansible/hosts ${path.module}/ansible/site.yml"
    }
}

resource "aws_route53_record" "jenkins" {
      zone_id = "${var.public_zone_id}"
      name = "jenkins"
      type = "CNAME"
      ttl = "30"
      records = ["${aws_instance.jenkins.public_dns}"]
}
