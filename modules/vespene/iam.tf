resource "aws_iam_role" "vespenerole" {
  name = "${var.environment}_vespene_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "vespene_profile" {
  name = "${var.environment}_vespene_profile"
  role = "${aws_iam_role.vespenerole.name}"
}

resource "aws_iam_role_policy_attachment" "vespene_ec2" {
  role       = "${aws_iam_role.vespenerole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_role_policy_attachment" "vespene_s3" {
  role       = "${aws_iam_role.vespenerole.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
