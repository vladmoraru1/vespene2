#!/bin/bash

sudo mv 0_common.sh ~/vespene/setup/0_common.sh
sudo mv 1_prepare.sh ~/vespene/setup/1_prepare.sh
sudo mv 2_database.sh ~/vespene/setup/2_database.sh
sudo mv 3_application.sh ~/vespene/setup/3_application.sh
cd vespene/setup
bash 1_prepare.sh
bash 2_database.sh
bash 3_application.sh
sudo -u postgres psql -c "drop database vespene;"
sudo -u postgres psql -c "create database vespene;"
sudo -u postgres psql vespene < vespene_db.bak
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('${username}', '${email}', '${password}')" | /usr/bin/python3.6 /opt/vespene/manage.py shell
bash 6_services.sh
