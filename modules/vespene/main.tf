data "template_file" "setup" {
  template = "${file("${path.module}/templates/setup.tpl")}"

  vars {
    username = "${var.username}"
    email    = "${var.email}"
    password = "${var.password}"
  }
}

resource "local_file" "setup" {
    content     = "${data.template_file.setup.rendered}"
    filename = "${path.module}/config/setup.sh"
}

resource "aws_instance" "vespene" {
    ami = "${var.image_id}"
    vpc_security_group_ids = [ "${aws_security_group.vespene_sg.id}" ]
    instance_type = "${var.instance_type}"
    key_name      = "${var.key_name}"
    iam_instance_profile        = "${aws_iam_instance_profile.vespene_profile.name}"
    associate_public_ip_address = true
    tags {
        Name = "${var.environment}-vespene"
    }
    subnet_id = "${element(var.public_subnets_id, count.index)}"

    depends_on = ["aws_instance.vespene"]

    connection {
      user         = "ec2-user"
      private_key  = "${file("~/.aws/vlad.pem")}"
    }

    provisioner "remote-exec" {
      inline = [
        "sudo yum -y install git jq",
        "git clone https://github.com/vespene-io/vespene.git /home/ec2-user/vespene",
        "curl -O https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_linux_amd64.zip",
        "sudo unzip terraform_0.11.10_linux_amd64.zip -d /usr/bin"
      ]
    }

    provisioner "file" {
      source = "${path.module}/config/setup.sh"
      destination = "/home/ec2-user/setup.sh"
    }

    provisioner "file" {
      source = "${path.module}/config/0_common.sh"
      destination = "/home/ec2-user/0_common.sh"
    }

    provisioner "file" {
      source = "${path.module}/config/1_prepare.sh"
      destination = "/home/ec2-user/1_prepare.sh"
    }

    provisioner "file" {
      source = "${path.module}/config/2_database.sh"
      destination = "/home/ec2-user/2_database.sh"
    }

    provisioner "file" {
      source = "${path.module}/config/3_application.sh"
      destination = "/home/ec2-user/3_application.sh"
    }

    provisioner "file" {
      source = "${path.module}/config/vespene_db.bak"
      destination = "/home/ec2-user/vespene/setup/vespene_db.bak"
    }

    provisioner "remote-exec" {
      inline = [
        "echo \"vespene ALL=(ALL) NOPASSWD:ALL\" | sudo tee --append /etc/sudoers.d/90-cloud-init-users",
        "chmod +x setup.sh",
        "./setup.sh"
      ]
    }
}

resource "aws_route53_record" "vespene" {
      zone_id = "${var.public_zone_id}"
      name = "vespene"
      type = "CNAME"
      ttl = "30"
      records = ["${aws_instance.vespene.public_dns}"]
}

# "sudo openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -subj \"/C=UK/ST=London/L=London/O=AL/OU=dev/CN=\"${aws_instance.vespene.public_dns}\"/EA=vlad@al.com\" -out vespene.crt -keyout vespene.key"
