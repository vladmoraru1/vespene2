resource "aws_security_group" "vespene_sg" {
    name = "${var.environment}-vespene-sg"
    description = "vespene security group"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    ingress {
        from_port = 8000
        to_port = 8000
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    ingress {
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["77.108.144.180/32"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${var.vpc_id}"

    tags {
      Name        = "${var.environment}-vespene-sg"
      Environment = "${var.environment}"
    }
}
