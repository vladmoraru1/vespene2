**Vespene documentation**  
[https://docs.vespene.io/index.html](https://docs.vespene.io/index.html)

**Starting vespene**  
Launch with:  
```./load.sh``` This will prompt you to enter the name, email and username used to sign in to the vespene web page.  
```source mkenv.sh``` This will export the relevant credentials into terraform  
```terraform init```  
```terraform plan```  
```terraform apply -auto-approve```  
You can then go to vespene at the following address:  
[http://www.vespene.grads.al-labs.co.uk:8000/](http://vespene.grads.al-labs.co.uk:8000/)  

**Database**  
When vespene is created by default it creates a postgresl database on the local machine. This database is where all of the config and secrets of the automation server are saved. If you were to connect your own database with pre written project configs you will have to alter the ```0_common.sh``` file can be found in ```/modules/vespene/config```

**Database dump**  
If you want to terminate this vespene instance but then run the same again then you can use this command to export the database as a sql script ```./scripts/dump_database.sh```
You will have to change ```key_path``` to where your ssh key is that you used to start the vespene instance.  

**Worker pools**  
If you want to run a project it must have a worker assigned and enabled to it.  
To enable a worker you must ssh into the instance and run the following commands:  
```cd /opt/vespene```  
```ssh-agent python manage.py worker <worker-pool-name>```  
The concept of worker pools is that at the start of the day the admin of the server enables the worker pools for the relevant users eg. dev/qa/prod and then at the end of the day the worker pools are destroyed.  
Creating a worker without being enabled by the admin will not run and leave the project queued.
