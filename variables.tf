variable "access_key" {}
variable "secret_key" {}
variable "username" {}
variable "email" {}
variable "password" {}

variable "region" {
  description = "The region of the infrastucture"
  default = "eu-west-2"
}

variable "environment" {
  description = "The environment"
  default = "vlommy"
}

variable "image_id" {
  default = "ami-0274e11dced17bb5b"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  description = "The public key for the bastion host"
  default = "vlad"
}

variable "vpc_id" {
  default = "vpc-7738c91e"
}

variable "public_subnets_id" {
  type        = "list"
  description = "The CIDR block for the public subnet"
  default = [
    "subnet-b6bfa5ce",
    "subnet-0de6f5120aa897fd2",
    "subnet-8a08f3e3"
  ]
}

variable "public_zone_id" {
  default = "Z2U00Q95U7EKEA"
}

variable "vpc_cidr" {
  description = "The CIDR block of the vpc"
  default = "10.0.0.0/16"
}

variable "public_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the public subnet"
  default = [
    "10.0.100.0/24",
    "10.0.101.0/24"
  ]
}

variable "private_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the private subnet"
  default = [
    "10.0.200.0/24",
    "10.0.201.0/24"
  ]
}

variable "availability_zones" {
  type        = "list"
  description = "The az that the resources will be launched"
  default = [
    "eu-west-2a",
    "eu-west-2b"
  ]
}
